<!DOCTYPE html>
<html lang="en">
  	<head>
  	<!-- 
		// Designer & Developer: M Nazim Uddin Molla;
		// E-mail: mohnazimuddin@gmail.com;
		// Phone: +8801711062063, +8801515243401;
		// Facebook: www.facebook.com/nzm404;
		// Gitlab: https://gitlab.com/mnazimmolla;
		// Github: https://github.com/mnazimmolla;
  	-->	
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Musco IT</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/modenizer.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
		<!-- Testmonial -->
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/testmonial.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
		<!-- Slick CSS -->
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/infinite-slider.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/nav.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
        <?php wp_head();?>
    </head>
    <body <?php body_class();?> >
 
		<div id="preloader">
		  <div id="status">&nbsp;</div>
		</div>

    	<!-- Header Part -->
	    <div class="header row">
	    	<div class="container-fluid">
	    		<div class="col-md-12 col-xs-12 col-sm-12">
				    <header>
				    	<div class="">
					        <nav>
					            <div class="logo-section">
					                <a href="<?php echo home_url();?>" class="logo">MUSCO IT</a>
					                <button class="hb-button"><i class="fa fa-bars"></i></button>
					            </div>
<!-- 					            <ul>
					                <li><a href="blog.php">blog</a></li>
					                <li><a href="contact.php">contact</a></li>
					                <li><a href="client-partner.php">clients & partners</a></li>
					                <li><a href="team.php">our team</a></li>
					                <li><a href="services.php">services</a></li>
					                <li><a href="portfolio.php">portfolio</a></li>
					                <li><a href="about-us.php">about us</a></li>
					                <li><a href="index.php">home</a></li>
					            </ul> -->
								 <?php
									wp_nav_menu( array(
										'menu'              => 'header_menu',
										'theme_location'    => 'header_menu',
										'depth'             => 3,
										'container'         => 'ul',
										'container_class'   => '',
										//'container_id'      => 'bs-example-navbar-collapse-1',
										//'menu_class'        => 'nav navbar-nav',
										//'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
										//'walker'            => new WP_Bootstrap_Navwalker())
									));
								?>					            
					        </nav>
				        </div>
				    </header>	    			
	    		</div>
	    	</div>
	    </div>
	    <!-- Header Part -->

	    <div class="clearfix">
		    <br>
		    <br>
	    </div>