<?php
function add_css_js()
{
	//Add Stylesheet
	// wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '4.4.0', 'all');
	// wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '3.3.7', 'all');
	// wp_enqueue_style('bootstrap-theme', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', array(), '3.3.7', 'all');
	// wp_enqueue_style('animate-css', '//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css', array(), '3.3.7', 'all');
	// wp_enqueue_style('montserrat-font', '//fonts.googleapis.com/css?family=Montserrat', array(), '3.3.7', 'all');
	// wp_enqueue_style('testmonial', get_template_directory_uri().'/assets/css/testmonial.css', array(), '1.0.0', 'all');
	// wp_enqueue_style('infinite-slider', get_template_directory_uri().'/assets/css/infinite-slider.css', array(), '1.0.0', 'all');
	// wp_enqueue_style('nav', get_template_directory_uri().'/assets/css/nav.css', array(), '1.0.0', 'all');
	// wp_enqueue_style('style', get_template_directory_uri().'/assets/css/style.css', array(), '1.0.0', 'all');

	// //Add Javascript
	// wp_enqueue_script('modenizer', get_template_directory_uri().'/assets/js/modenizer.js', array('jquery'), '1.0.0', false);
	// wp_enqueue_script('modernizr', get_template_directory_uri().'/assets/js/modernizr.js', array('jquery'), '1.0.0', false);
	// wp_enqueue_script('nav-js', get_template_directory_uri().'/assets/js/nav.js', array('jquery'), '1.0.0', true);
	// wp_enqueue_script('wow-js', get_template_directory_uri().'//cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js', array('jquery'), '1.1.2', true);
	// wp_enqueue_script('slick-js', get_template_directory_uri().'//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js', array('jquery'), '1.6.0', true);
	// wp_enqueue_script('slick-carousel', get_template_directory_uri().'/assets/js/slick-carousel.js', array('jquery'), '1.0.0', true);
	// wp_enqueue_script('script', get_template_directory_uri().'/assets/js/script.js', array('jquery'), '1.0.0', true);
	// wp_enqueue_script('masonry.pkgd.min', get_template_directory_uri().'/assets/js/masonry.pkgd.min.js', array('jquery'), '1.0.0', true);
	// wp_enqueue_script('flexslider', get_template_directory_uri().'/assets/js/jquery.flexslider-min.js', array('jquery'), '1.0.0', true);
	// wp_enqueue_script('main-js', get_template_directory_uri().'/assets/js/main.js', array('jquery'), '1.0.0', true);
	// wp_enqueue_script('wow-start', get_template_directory_uri().'/assets/js/wow.js', array('jquery'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'add_css_js');
?>