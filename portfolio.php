<?php
/*
*
*Template Name: Portfolio
*
*/
?>
<?php get_header();?>

	    <div class="portfolio-page-bg">
	    	<p><?php echo cs_get_option('portfolio_section_heading');?></p>
	    </div>

	    <div class="portfolio-page wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    	<div class="container">
	    		<div class="row">

	    			<?php
	    				$portfolio = cs_get_option('portfolio_item');
	    				if(is_array($portfolio))
	    				{
	    					foreach($portfolio as $key => $value)
	    					{ ?>

			    			<div class="col-md-4 col-xs-12 col-sm-6">
			    				<div class="portfolio-item">
			    					<a href="<?php echo $value['portfolio_item_link'];?>" target="_Blank">
				    					<img src="<?php echo wp_get_attachment_image_src( $value['portfolio_item_image'], 'full')[0]; ?>" alt="portfolio image" class="img-responsive">
				    					<br>
				    					<p class="port-item-title"><?php echo $value['portfolio_item_heading'];?></p>
				    					<p class="port-item-desc"><?php echo $value['portfolio_item_desc'];?></p>
			    					</a>
			    				</div>
			    			</div> 

	    				<?php	}
	    				}
	    			?> 

	    		</div>
	    	</div>
	    </div>

<?php get_footer();?>