<?php
/*
*
*Template Name: Team
*
*/
?>
<?php get_header();?>

	    <div class="team-page-bg">
	    	<p><?php echo cs_get_option('team_sectin_heading');?></p>
	    </div>

	    <div class="team-member wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    	<div class="container-fluid">
		    	<div class="row">
		    		<?php 
		    			$team = cs_get_option('team_member');
		    			if(is_array($team))
		    			{
		    				foreach($team as $key => $val)
		    				{ ?>

				    		<div class="col-md-3 col-sm-6 col-xs-12">
				    			<div class="team-item">
				    				<img src="<?php echo wp_get_attachment_image_src( $val['member_image'], 'full')[0]; ?>" class="img-responsive" alt="Team Member Image">
				    				<br>
				    				<p class="member-name"><?php echo $val['member_name'];?></p>
				    				<p><?php echo $val['member_position'];?></p>
				    				<p class="team-social-media">
				    					<a href="<?php echo $val['member_fb'];?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				    					<a href="<?php echo $val['member_tw'];?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				    					<a href="<?php echo $val['member_link'];?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				    					<a href="<?php echo $val['member_git'];?>"><i class="fa fa-github" aria-hidden="true"></i></a>
				    				</p>
				    			</div>
				    		</div>

		    			<?php	}
		    			}
		    		?>

		    				    				    		    		
		    	</div>
	    	</div>
	    </div>

<?php get_footer();?>