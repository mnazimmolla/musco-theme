<?php
/*
*
*Template Name: Blog
*
*/
?>
<?php get_header();?>

	    <div class="blog-page-bg">
	    	<p>get more about us</p>
	    </div>

	    <div class="blog-page">
	    	<div class="container">
	    		<div class="row wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    	    	<?php
							if ( have_posts() ) : while ( have_posts() ) : the_post();
					?>
	    	    	<div class="col-md-4 col-sm-6 col-xs-12">
		    			<div class="blog-item">
		    				<a href="<?php the_permalink();?>">
		    					<!-- <img src="" class="img-responsive" alt="Blog Image"> -->
		    					<?php the_post_thumbnail('single_img');?>
		    				</a>
		    				<br>
		    				<p class="blog-heading"><a href="<?php the_permalink();?>"><?php the_title(); ?></a></p>
		    				<p class="blog-date">Date: <span><?php the_date('d-m-Y'); ?></span></p>
		    				<p class="blog-desc">
		    					<?php the_content();?>
		    				</p>
		    				<button onClick="parent.location='<?php the_permalink();?>'" class="btn">read more</button>
		    			</div>	    	    		
	    	    	</div> 
	    	    	<?php endwhile; endif;?>	
	    		</div>
	    	</div>
	    </div>

<?php get_footer();?>