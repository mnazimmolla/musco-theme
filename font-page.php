<?php
/*
*
*Template Name: Front Page
*
*/	
?>
<?php get_header();?>

		<!-- Hero Area Part -->
	    <div class="hero-banner">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-sm-8 col-md-8 col-xl-8 col-xs-12 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1s">
	    				<div style="color: #333">
	    					<p class="hero-promo"><?php echo cs_get_option('promotional_text');?><br>
	    						<span class="org"><?php echo cs_get_option('promotional_text_bold');?></span></p>
	    					<button class="btn">see our portfolio</button>
	    				</div>
	    			</div>	
	    			<div class="col-sm-4 col-md-4 col-xl-4 col-xs-12 wow fadeInRight" data-wow-duration="2s" data-wow-delay="1s">
	    				<div class="services-text">
		    				<p><?php echo cs_get_option('home_service_title');?></p>
		    				<?php
		    					$ser_items = cs_get_option('home_service');
		    					if(is_array($ser_items))
		    					{
		    						foreach ($ser_items as $key => $value) 
		    						{ ?>
		    							<p><i class="<?php echo cs_get_option('home_service_icon');?>" aria-hidden="true"></i> <?php echo $value['home_service_item'];?></p>		
		    					<?php	}
		    					}
		    				?>
		    				

	    				</div>
	    			</div>		
	    		</div>
	    	</div>
	    </div>  
		<!-- Hero Area Part -->

		<!-- Home Client Part -->
		<div class="home-client">	
			<div class="container-fluid">
				<div class="home-client-heading">
					<p><?php echo cs_get_option('client_heading');?></p>
				</div>
				<section class="customer-logos slider">
					

					<?php
						$client = cs_get_option('home_cli_par_logos');
						if(is_array($client))
						{
							foreach ($client as $key => $value) 
								{ ?>
								<div class="slide"><img class="customer-logos-item" src="<?php echo wp_get_attachment_image_src( $value['home_client_logo'], 'full')[0]; ?>"></div>								
						<?php	}
						}
					?>
				               
				
				</section>
			</div>
		</div>
		<!-- Home Client Part -->

		<!-- Home Promotinal Banner Part -->
		<div class="promotional-banner wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
						<p><?php echo cs_get_option('promotional_text_pro');?></p>
						<button class="btn">contact us</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Home Promotinal Banner Part -->

		<!-- Home Partner Part -->
		<div class="home-partner">	
			<div class="container-fluid">
				<div class="home-partner-heading">
					<p><?php echo cs_get_option('partner_heading');?></p>
				</div>
				<section class="customer-logos slider">
				<?php 
					$par_sec = cs_get_option('home_par_logos');
					if(is_array($par_sec))
					{
						foreach ($par_sec as $key => $value) 
						{ ?>
							
					<div class="slide"><img class="partner-logos-item" src="<?php echo wp_get_attachment_image_src( $value['home_par_logo'], 'full')[0]; ?>" alt="Partner Logos"></div>							

					<?php	}
					}
				?>
						
				</section>
			</div>
		</div>
		<!-- Home Partner Part -->

		<!-- Client Testmonial Part -->
		<div class="client-testmonials wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
			<div class="cd-testimonials-wrapper cd-container">
				<p class="client-test-heading"><?php echo cs_get_option('home_testomonial_heading');?></p>
				<ul class="cd-testimonials">
					<?php
						$testmonial = cs_get_option('home_testomonial_group');
						if(is_array($testmonial))
						{
							foreach($testmonial as $key => $value)
							{ ?>
								<li>
									<p><?php echo $value['testmonail_item'];?></p>
									<div class="cd-author">
										<img src="<?php echo wp_get_attachment_image_src($value['testmonail_pic'], 'full')[0];?>" alt="Author image">
										<ul class="cd-author-info">
											<li><?php echo $value['testmonail_name'];?></li>
											<li><?php echo $value['testmonail_position'];?>, <?php echo $value['testmonail_company'];?></li>
										</ul>
									</div>
								</li>								
						<?php	}
						}
					?>


				</ul>
			</div>					
		</div>
		<!-- Client Testmonial Part -->

<?php get_footer();?>