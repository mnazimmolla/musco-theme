		<div class="footer">
			<footer>
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xl-12 col-lg-12 footer-social-media">
							<?php 
								$icon = cs_get_option('social_media');
								if(is_array($icon))
								{
									foreach ($icon as $key => $value) 
									{ ?>
								<a href="<?php echo $value['social_media_href'];?>" target="_Blank"><i class="<?php echo $value['social_media_icon'];?>" aria-hidden="true"></i></a>
								<?php	}
								}
							?>
							

						</div>
					</div>
					
					<?php
						$contact = cs_get_option('footer_contact');
						if(is_array($contact))
						{
							foreach($contact as $key => $value)
							{ ?>
							<p class="footer-para"><?php echo $value['contact_type_heading'];?>: <?php echo $value['contact_type_info'];?></p>
						<?php	}
						}
					?>
		
					<hr>
					<?php 
					$year = date("Y");
					?>
					<p class="footer-para">Copyright <?php echo $year;?> by Musco IT | All Rights Reserved</p>
				</div>
			</footer>
		</div>
<?php wp_footer();?>
    </body>
    <!-- Jquery JS -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-3.2.1.min.js"></script>
    <!-- Nav Bar JS -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/nav.js"></script>
    <!-- Wow JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
    <!-- Slick Carousel JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
	<!-- Slick Carousel Active JS -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/slick-carousel.js"></script>
    <!-- Script JS -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>

	<!-- Testimonial Slider -->
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/masonry.pkgd.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.flexslider-min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
	<script>
        new WOW().init();
    </script>

</html>