<?php
/*
*
*Template Name: Contact
*
*/
?>
<?php get_header();?>

	    <div class="contact-from-bg">
	    	<p>Feel free to contact us.</p>
	    </div>

	    <div class="contact-from wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    	<div class="row">
	    		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
	    		</div>
	    		<div class="col-md-6 col-sm-6 col-lg-6 col-xs-6">
					<form>
					  <div class="form-group">
					    <input type="text" class="form-control" id="name" placeholder="Your Name">
					  </div>
					  <div class="form-group">
					    <input type="email" class="form-control" id="email" placeholder="Your E-mail">
					  </div>
					  <div class="form-group">
					    <input type="text" class="form-control" id="subject" placeholder="Your Subject">
					  </div>
					  <div class="form-group">
					    <textarea class="form-control" placeholder="Your Message" rows="7" name="message" id="message"></textarea>
					  </div>					  
					  <button type="submit" class="btn btn-default">send message</button>
					</form>	    			
	    		</div>
	    		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
	    		</div>
	    	</div>
	    </div>

<?php get_footer();?>