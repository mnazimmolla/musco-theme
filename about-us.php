<?php
/*
*
* Template Name: About US
*
*/
?>
<?php get_header();?>

	    <div class="about-page-bg">
	    	<p>welcome to the musco it</p>
	    </div>

	    <div class="about-text-top container">
	    	<p>
<b>MUSCO IT Services Limited, miTS</b> in short, is a proud member of the eminent MUSCO family and is one of the leading IT solution & service providers in Bangladesh specializing in providing a wide range of software solutions & end to end technology service management.	
	    	</p>
	    	<p>
Since the inception, miTS has been providing quality IT solutions to various organizations and solving their problems of aligning IT with business directions. We offer Software Solution Development & Deployment Services, Technology Outsourcing Services, Consultancy Services, and Managed IT Services which include Infrastructure Management, Deployment, and System Integration services to the clients. We currently focus on Financial Sector, Education Sector, Development Sector, SME’s, Manufacturing & Service sectors of Bangladesh.
	    	</p>
	    	<p>
<b>miTS</b> is one of the largest IT based service providing organizations in Bangladesh , highly qualified, efficient and innovative employees hand-picked by the management of the organization. We are committed to help our clients to achieve operational efficiency through transforming their existing operations by using our best valued solutions and services. We deliver world class solutions and services to our customers. We are committed to Quality Management as per ISO/IEC 9001 QMS guideline. Our international quality IT solutions and service are available at the most convenient way so that large, medium & even small enterprises can improve their productivity and day to day operational activities.	    		
	    	</p>
	    </div>

	    <div class="about-us-page wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    	<div class="container">
		    	<div class="row">
		    		<div class="col-md-12 col-sm-12 col-xs-12">
		    			<div class="about-item">
		    				<p class="about-heading">Our vision</p>
		    				<p class="about-desc">
To contribute in overall development of our country by providing available IT services for the people of all strata with simple condition and dedicated services and to be a mentor organization for the development of IT sector in Bangladesh and global platform.
		    				</p>
		    			</div>
		    		</div>
		    		<div class="col-md-12 col-sm-12 col-xs-12">
		    			<div class="about-item">
		    				<p class="about-heading">Mission</p>
		    				<p class="about-desc">
								<b>We strive to achieve the following goals:</b>
								<ul>
									<li>To be State of the Art in our technical Process.</li>
									<li>To implement the best Integrity, Capability & Maturity models.</li>
									<li>To motivate all categories of people to use the IT facilities for more productivity.</li>
									<li>To motivate young generation for using IT to develop their creativity.</li>
									<li>Provide IT service with maximum quality and minimum cost.</li>
									<li>To take potential steps for development of IT sector in home and abroad.</li>
									<li>To be a strong player in Software Solutions space offering great value proposition to international clients. </li>
								</ul>
		    				</p>
		    			</div>
		    		</div>	
		    		<div class="col-md-12 col-sm-12 col-xs-12">
		    			<div class="about-item">
		    				<p class="about-heading">values</p>
		    				<p class="about-desc">
							<ul>
								<li>Caring</li>
								<li>Reliable</li>
								<li>Innovative</li>
								<li>Simple</li>
								<li>Positive</li>
							</ul>
		    				</p>
		    			</div>
		    		</div>
		    		<div class="col-md-12 col-sm-12 col-xs-12">
		    			<div class="about-item">
		    				<p class="about-heading">Corporate philosophy</p>
		    				<p class="about-desc">
								<b>To bring about positive differences to clients across all operations by delivering service and solution excellence.</b>
								<ul>
									<li>Understand Client’s Business & Philosophies</li>
									<li>Investigate Business Performance</li>
									<li>Locate Business Bottlenecks & Inefficiencies</li>
									<li>Provide Appropriate & Efficient System Solution</li>
									<li>Deliver Customer Satisfaction and Goodwill</li>
								</ul>
		    				</p>
		    			</div>
		    		</div>	
		    		<div class="col-md-12 col-sm-12 col-xs-12">
		    			<div class="about-item">
		    				<p class="about-heading">Why miTS</p>
		    				<p class="about-desc">
								<ul>
									<li>Quality Service Assurance</li>
									<li>State of the Art Data Center</li>
									<li>24×7 Customer Service</li>
									<li>15+  Expert Resources</li>
									<li>Efficient Project Management</li>
									<li>Updated Technologies</li>
									<li>Corporate Recognition</li>
									<li>Corporate Social Responsibilities</li>
									<li>Industry Experience</li>
									<li>Business Ethics & Practices</li>
								</ul>
		    				</p>
		    			</div>
		    		</div>		    				    				    			    		
		    	</div>
	    	</div>
	    </div>

<?php get_footer();?>