<?php get_header();?>



	    <div class="blog-page-bg">
	    	<p>get more about us</p>
	    </div>

	    <div class="blog-page">
	    	<div class="container">
	    		<div class="row wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    	    		<?php
							if ( have_posts() ) : while ( have_posts() ) : the_post();
						?>



	    	    	<div class="col-md-4 col-sm-6 col-xs-12">
		    			<div class="blog-item">
		    				<a href="<?php the_permalink();?>">
		    					<img src="https://www.lavalux.com/wp-content/uploads/2017/10/blog.jpg" class="img-responsive" alt="Blog Image">
		    				</a>
		    				<br>
		    				<p class="blog-heading"><a href="<?php the_permalink();?>">ULTRA MODERN DESIGN</a></p>
		    				<p class="blog-date">Date: <span>27.01.2018</span></p>
		    				<p class="blog-desc">
		    					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
		    				</p>
		    				<button class="btn">read more</button>
		    			</div>	    	    		
	    	    	</div> 
<?php endwhile; endif;?>
	

	    	    	    	    	   		    	    		    	    	    	    		   	
	    		</div>
	    	</div>
	    </div>

<?php get_footer();?>