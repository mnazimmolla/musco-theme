<?php get_header();?>

	    <div class="blog-single-page-bg">
	    	<p>get more about us</p>
	    </div>

	    <div class="blog-single-page">
	    	<div class="container">
	    		<div class="row wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    	    	 
	    	    	 <?php while ( have_posts() ) : the_post(); ?>
	    	    	 <p class="blog-heading"><?php the_title();?></p>  
	    	    	 <p class="blog-date">Date: <span><?php the_time('d. m .Y');?></span></p> 	    	   		    	    		    	    	  
	    	    	 <div class="img-responsive">
	    	    	 	<?php the_post_thumbnail();?>
	    	    	 </div>
	    	    	 <p class="blog-desc">
	    	    	 	<?php the_content();?>
		    		</p>
		    	<?php endwhile;?>
	    		</div>
	    	</div>
	    </div>

<?php get_footer();?>