<?php
/*
*
*Template Name: Service
*
*/
?>
<?php get_header();?>

	    <div class="service-page-bg">
	    	<p><?php echo cs_get_option('service_section_heading');?></p>
	    </div>

	    <div class="service-page">
	    	<div class="container">
	    		<div class="row wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">    
	    			<?php
	    			$services = cs_get_option('service_item');
	    			if(is_array($services))
	    			{
	    				foreach($services as $key => $val)
	    				{ ?>

	    	  		<div class="col-md-6 col-sm-6 col-xs-12">
	    	  			<div class="service-item">
	    	  				<p class="service-icon"><i class="fa fa-3x <?php echo $val['service_item_icon'];?>" aria-hidden="true"></i></p>
	    	  				<p class="service-heading"><?php echo $val['service_item_title'];?></p>
	    	  				<p class="service-desc"><?php echo $val['service_item_desc'];?></p>
	    	  			</div>
	    	  		</div>

	    			<?php	}
	    			}
	    			?>

	    		</div>
	    	</div>
	    </div>

<?php get_footer();?>