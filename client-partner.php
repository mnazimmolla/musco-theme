<?php 
/*
*
*Template Name: Client & Partent
*
*/
?>
<?php get_header();?>

	    <div class="client-partner-page-bg">
	    	<p><?php echo cs_get_option('client_page_heading');?></p>
	    </div>

	    <div class="client-partner-page">
	    	<div class="container">
	    		<div class="row wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
	    			<p class="clients-heading"><?php echo cs_get_option('client_heading_cli');?></p>
	    			<?php
	    				$client = cs_get_option('client_logo_cli');
	    				if(is_array($client))
	    				{
	    					foreach($client as $key => $value)
	    					{ ?>

			    			<div class="client-logo col-md-3 col-sm-6 col-xs-12">
			    				<img src="<?php echo wp_get_attachment_image_src( $value['cli_logo_item'], 'full')[0]; ?>" class="img-responsive">
			    			</div>

	    				<?php	}
	    				}
	    			?>


	    		</div>
	    		<div class="row partners wow fadeInUp" data-wow-duration="2s" data-wow-delay="2s">
	    			<p class="clients-heading"><?php echo cs_get_option('partner_heading_part');?></p>
	    			<?php 
	    				$partners = cs_get_option('partner_logo_part');
	    				if(is_array($partners))
	    				{
	    					foreach($partners as $key => $value)
	    					{ ?>

			    			<div class="client-logo col-md-3 col-sm-6 col-xs-12">
			    				<img src="<?php echo wp_get_attachment_image_src( $value['partner_logo_item'], 'full')[0]; ?>" class="img-responsive">
			    			</div>

	    				<?php	}
	    				}
	    			?>

  	    			
	    		</div> 
	    	</div>
	    </div>

<?php get_footer();?>